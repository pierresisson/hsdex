Rails.application.routes.draw do
  devise_for :users
  resources :races

  resources :types

	root 'home#index'
	resources :cartes
end

  