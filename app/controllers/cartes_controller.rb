class CartesController < ApplicationController
	before_action :set_carte, only: [:show, :edit, :update, :destroy]
	
	def index
		@cartes = Carte.paginate(page: params[:page], per_page: 3).includes(:type).order(number: :asc)
	end


	def show
	end
	
	def new
		@carte = Carte.new
	end
	
	def create
		@carte = Carte.new carte_params
		if @carte.save
			flash[:sucess] = "La Carte #{@carte.name} a bien été ajoutée"
			redirect_to @carte
		else 
			render 'new'
		end
	end
	
	def edit
	end
	
	def update
		if @carte.update carte_params
			flash[:sucess] = "La Carte #{@carte.name} a bien été mise à jour"
			redirect_to @carte
		else
			render 'edit'
		end
	end
	
	def destroy
		@carte.destroy
		flash[:alert] = "La Carte #{@carte.name} a bien été supprimée"
		redirect_to cartes_path
	end
	
	private
	
	def set_carte
		@carte = Carte.includes(:races).find (params[:id])
	end
	
	def cartes_params 
		params.require(:carte).permit(:name, :number, :mana, :rarete, :type_id)
	end
	
	def carte_params
		params.require(:carte).permit(
			:name,
			:number,
			:mana,
			:rarete,
			:type_id,
			race_ids: []
		)
	end
end