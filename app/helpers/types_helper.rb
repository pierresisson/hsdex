module TypesHelper 
	def type_badge(type, name = nil) 
		name ||= type.name
		raw link_to type_badge_link(name, type.color), type_path(type.id) 
	end 
	
	def type_badge_link(name, color) 
		raw "<span style='background-color:#{color}' class='badge'> 
		#{name} 
		</span>" 
	end 
end