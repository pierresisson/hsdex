class Race < ActiveRecord::Base
  belongs_to :type
  has_many :carte_races
  has_many :cartes, through: :carte_races
end
