class Carte < ActiveRecord::Base
	belongs_to :type
	has_many :carte_races
	has_many :races, through: :carte_races
end