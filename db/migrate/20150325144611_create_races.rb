class CreateRaces < ActiveRecord::Migration
  def change
    create_table :races do |t|
      t.string :name
      t.string :description
      t.references :type, index: true

      t.timestamps null: false
    end
    add_foreign_key :races, :types
  end
end
