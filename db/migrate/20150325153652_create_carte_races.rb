class CreateCarteRaces < ActiveRecord::Migration
  def change
    create_table :carte_races do |t|
      t.references :carte, index: true
      t.references :race, index: true

      t.timestamps null: false
    end
    add_foreign_key :carte_races, :carte
    add_foreign_key :carte_races, :race
  end
end
