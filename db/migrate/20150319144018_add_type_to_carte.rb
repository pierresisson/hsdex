class AddTypeToCarte < ActiveRecord::Migration
  def change
    add_reference :cartes, :type, index: true
    add_foreign_key :cartes, :types
  end
end
