class AddAttributesToCartes < ActiveRecord::Migration
  def change
	  add_column :cartes, :number, :integer
	  add_column :cartes, :level, :integer
	  add_column :cartes, :mana, :integer
	  add_column :cartes, :rarete, :integer
  end
end
